﻿import QtQuick 2.0
import QtQuick.Controls 2.0

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("QtTest")

    TextField {
        id: textField
        x: 77
        y: 78
        text: qsTr("Text Field")
    }

    RadioButton {
        id: radioButton
        x: 45
        y: 46
        text: qsTr("Radio Button")
    }

    ComboBox {
        id: comboBox
        x: 45
        y: 108
    }

    Label {
        id: label
        x: 45
        y: 83
        text: qsTr("Label")
    }

    ProgressBar {
        id: progressBar
        x: 45
        y: 138
    }

    Button {
        id: button
        x: 45
        y: 168
        text: qsTr("Button")
    }

    Button {
        id: button1
        x: 45
        y: 198
        text: qsTr("Button")
    }

    Button {
        id: button2
        x: 45
        y: 228
        text: qsTr("Button")
    }

    CheckBox {
        id: checkBox
        x: 45
        y: 258
        text: qsTr("Check Box")
    }
}
