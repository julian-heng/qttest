﻿using System;
using System.IO;

using NLog;

using Qml.Net;
using Qml.Net.Runtimes;


namespace QtTest
{
    static class Program
    {
        private static Logger Log { get; } = NLog.LogManager.GetCurrentClassLogger();


        static int Main(string[] args)
        {
            string qtQuickControls;
            SetupNLog();

            qtQuickControls = Path.Combine(".", "qtquickcontrols2.conf");
            Log.Info($"Using qt quick controls config: \"{qtQuickControls}\"");

            RuntimeManager.DiscoverOrDownloadSuitableQtRuntime();

            Qt.PutEnv("QT_QUICK_CONTROLS_CONF", qtQuickControls);

            using (QGuiApplication app = new QGuiApplication(args))
            {
                using (QQmlApplicationEngine engine = new QQmlApplicationEngine())
                {
                    engine.Load("program.qml");
                    return app.Exec();
                }
            }
        }


        static void SetupNLog()
        {
            NLog.Config.LoggingConfiguration conf = new NLog.Config.LoggingConfiguration();
            conf.AddRule(LogLevel.Info, LogLevel.Fatal, new NLog.Targets.ConsoleTarget());
            NLog.LogManager.Configuration = conf;
        }
    }
}
